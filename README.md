# No maintenance. Do not use. (1 Feb 2018)

## Installer Pack for activate opkg 

1. download `pack.tar.bz2` from Tags menu.
2. save to USB
3. plug USB to CAR
4. SSH to CAR

###  Install
```bash
cd /tmp/mnt/sda
tar xf pack.tar.bz2
cd pack
sh install.sh
reboot
```


###  Uninstall
```bash
cd /tmp/mnt/sda
tar xf pack.tar.bz2
cd pack
sh remove.sh
reboot
```
