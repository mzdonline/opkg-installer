#!/bin/sh


opkg remove autorun --force-removal-of-dependent-packages
opkg remove wget --force-removal-of-dependent-packages

mount -o rw,remount /
mount -o rw,remount /tmp/mnt/resources

rm -rf /etc/opkg
rm -rf /data_persist/dev/opkg
rm -rf /usr/lib/opkg
rm -rf /tmp/mnt/resources/aio

