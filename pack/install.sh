#!/bin/sh

current_dir=`pwd`
cd $current_dir

mount -o rw,remount /
mkdir -p /tmp/mnt/data_persist/dev/opkg
ln -sf /tmp/mnt/data_persist/dev/opkg /usr/lib/opkg
mkdir /etc/opkg
cd /etc/opkg
echo "src/gz dev https://downloads.sourceforge.net/project/cmu-opkg/dev
dest root /
dest ram /tmp
lists_dir ext /usr/lib/opkg/lists
" > /etc/opkg/opkg.conf

cd  $current_dir
opkg install $current_dir/autorun_1.3-1_arm.ipk
opkg install $current_dir/libuuid_1.0.3_arm.ipk
opkg install $current_dir/libressl_2.5.4_arm.ipk
opkg install $current_dir/wget_1.19-4_arm.ipk

echo "OPKG Activated"
